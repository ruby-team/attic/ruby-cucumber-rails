require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/cucumber/**/database_spec.rb'
  # FIXME skipping generator tests, which need
  # https://rubygems.org/gems/ammeter
end
